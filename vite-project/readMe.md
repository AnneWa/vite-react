# TODO #

Initialise un nouveau projet React avec npm et vite

Ouvre le dossier du projet, installe-le et exécute le serveur (les commandes sont affichées dans le terminal)

Ouvre le site sur le navigateur pour voir que tout fonctionne correctement

Modifie le fichier src/App.jsx et remplace le texte "Click on the Vite and React logos to learn more" par le texte "React est une bibliothèque/un framework" (en ne gardant que la partie de la phrase qui est vraie !)

Rafraîchis le site pour voir si la modification a été prise en compte

Versionne ton projet sur Gitlab et envoi le lien en solution